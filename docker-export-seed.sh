#!/usr/bin/env bash
# This script creates a new seed archive from the latest builder image
IFS=$'\n\t'
set -euo pipefail

cd "$(dirname "$(readlink -f "${0:-.}")")"

source ./common.sh
source ./config.sh

IMAGE_VERSION="$(get_image_version_from_image_name "${IMAGE_NAME}")"

echo "Creating seed '${SEED_PREFIX}${IMAGE_VERSION}${SEED_SUFFIX}'..."
# Just run a noop so we get a container that can be exported
docker run "${IMAGE_NAME}:${IMAGE_VERSION}" true
container_id="$(get_container_id_from_ancestor "${IMAGE_NAME}:${IMAGE_VERSION}")"
docker export "${container_id}" | xz -z -k -9 -e --threads=0 > "${SEED_PREFIX}${IMAGE_VERSION}${SEED_SUFFIX}"
docker rm "${container_id}" >/dev/null
sha256sum "${SEED_PREFIX}${IMAGE_VERSION}${SEED_SUFFIX}" > "${SEED_PREFIX}${IMAGE_VERSION}${SEED_SUFFIX}".sha256
gpg --armor --detach-sign "${SEED_PREFIX}${IMAGE_VERSION}${SEED_SUFFIX}"
echo "Seed is available as '${SEED_PREFIX}${IMAGE_VERSION}${SEED_SUFFIX}'"
