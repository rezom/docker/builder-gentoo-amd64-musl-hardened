DOCKER_DISTCC_ENABLED="0"
# Passed as /usr/bin/distcc-config --set-hosts
# e.g.: "localhost 192.168.0.1 192.168.0.2 192.168.0.3"
DOCKER_DISTCC_HOSTS="localhost"
# -jN -lM
# set the value of N to twice the number of total (local + remote) CPU cores + 1, and
# set the value of M to the number of local CPU cores
DOCKER_DISTCC_MAKEOPTS="-j13 -l8"
