#!/usr/bin/env bash
# This script is supposed to be run inside the docker context
# No dependencies should be sourced here
IFS=$'\n\t'
set -euo pipefail

# This is where the final output goes
DOCKER_TARGET_DIR="${DOCKER_TARGET_DIR:-/mnt/target}"
# The global gentoo USE flags
IFS=' ' read -r -a DOCKER_USE <<< "${DOCKER_USE:-}"
# Multiline configuration to append to the make.conf
DOCKER_APPEND_MAKE_CONF="${DOCKER_APPEND_MAKE_CONF:-}"
# Multiline packages with accept keywords that should be added to package.accept_keywords
DOCKER_PACKAGE_ACCEPT_KEYWORDS="${DOCKER_PACKAGE_ACCEPT_KEYWORDS:-}"
# Multiline packages with use flags that should be added to package.use
DOCKER_PACKAGE_USE="${DOCKER_PACKAGE_USE:-}"
# Multiline packages with unmask that should be added to package.unmask
DOCKER_PACKAGE_UNMASK="${DOCKER_PACKAGE_UNMASK:-}"
# The target packages to install
IFS=' ' read -r -a DOCKER_TARGET_PACKAGES <<< "${DOCKER_TARGET_PACKAGES:-}"
# The packages that should only be present in the builder
IFS=' ' read -r -a DOCKER_BUILDER_PACKAGES <<< "${DOCKER_BUILDER_PACKAGES:-}"
# The GLSAs to ignore
IFS=' ' read -r -a DOCKER_IGNORE_GLSAS <<< "${DOCKER_IGNORE_GLSAS:-}"
# makeopts
DOCKER_MAKEOPTS="${DOCKER_MAKEOPTS:--j$(($(nproc --all)+1))}"
# distcc
DOCKER_DISTCC_ENABLED="${DOCKER_DISTCC_ENABLED:-0}"
DOCKER_DISTCC_HOSTS="${DOCKER_DISTCC_HOSTS:-}"
DOCKER_DISTCC_MAKEOPTS="${DOCKER_DISTCC_MAKEOPTS:-}"

DOCKER_NEEDED_BASE_PACKAGES=(
	sys-apps/baselayout
	sys-libs/musl
	sys-apps/busybox
	)

if [[ "${#DOCKER_TARGET_PACKAGES[@]}" -eq 0 ]] ; then
	echo "No target packages specified, exiting..." 1>&2
	exit 0
fi

# Goes to /etc/portage/profile/packages
# unsets packages from the system set
function portage_profile_packages {
	cat << 'EOF'
# base system packages
# in busybox
-*app-arch/bzip2
# in busybox
-*app-arch/gzip
# in busybox
-*app-arch/tar
# in busybox
-*app-arch/xz-utils
# busybox sh
-*app-shells/bash:0
# busybox ping
-*net-misc/iputils
# not needed
-*net-misc/rsync
# in busybox
-*net-misc/wget
# busybox replaces this
-*sys-apps/coreutils
# not needed
-*sys-apps/diffutils
# not strictly needed
-*sys-apps/file
# not needed
-*>=sys-apps/findutils-4.4
# in busybox
-*sys-apps/gawk
# in busybox
-*sys-apps/grep
# no
-*sys-apps/kbd
# in busybox
-*sys-apps/less
# in busybox
-*sys-process/procps
# in busybox
-*sys-process/psmisc
# in busybox
-*sys-apps/sed
# in busybox
-*sys-apps/which
-*sys-devel/binutils
-*sys-devel/gcc
-*sys-devel/gnuconfig
-*sys-devel/make
-*>=sys-devel/patch-2.7
-*sys-fs/e2fsprogs
-*virtual/dev-manager
-*virtual/editor
-*virtual/man
-*virtual/modutils
-*virtual/os-headers
-*virtual/package-manager
-*virtual/pager
-*virtual/service-manager
-*virtual/shadow
-*virtual/ssh

# default/linux
-*sys-apps/iproute2
-*sys-apps/man-pages
-*sys-apps/net-tools
-*sys-apps/util-linux

# default/linux/musl
-*app-misc/pax-utils
-*sys-apps/sandbox

# features/hardened
-*sys-apps/elfix

# hardened/linux
-*sys-apps/paxctl
EOF
}

# Add better mirrors to the make.conf
echo 'GENTOO_MIRRORS="http://ftp.halifax.rwth-aachen.de/gentoo/ http://gentoo.mirrors.ovh.net/gentoo-distfiles/"' >> /etc/portage/make.conf

# Portage configurations
cat << 'EOF'
##########################
# Portage configurations #
##########################
EOF
echo "DOCKER_TARGET_DIR='${DOCKER_TARGET_DIR}'"
echo "DOCKER_TARGET_PACKAGES :"
echo "${DOCKER_TARGET_PACKAGES[*]}"
echo
echo "DOCKER_BUILDER_PACKAGES :"
echo "${DOCKER_BUILDER_PACKAGES[*]}"
echo
oIFS="${IFS}" ; IFS=' '
if [[ "${#DOCKER_USE[@]}" -gt 0 ]] ; then
	echo 'USE="''$''{USE} '"${DOCKER_USE[*]}"'"' >> /etc/portage/make.conf
	echo "USE='${DOCKER_USE[*]}'"
	echo
fi
mkdir -p /etc/portage/package.accept_keywords
if [[ -n "${DOCKER_PACKAGE_ACCEPT_KEYWORDS}" ]] ; then
	echo "${DOCKER_PACKAGE_ACCEPT_KEYWORDS}" >> /etc/portage/package.accept_keywords/specific
	echo "package.accept_keywords :"
	echo "${DOCKER_PACKAGE_ACCEPT_KEYWORDS}"
	echo
fi
mkdir -p /etc/portage/package.use
if [[ -n "${DOCKER_PACKAGE_USE}" ]] ; then
	echo "${DOCKER_PACKAGE_USE}" >> /etc/portage/package.use/specific
	echo "package.use :"
	echo "${DOCKER_PACKAGE_USE}"
	echo
fi
mkdir -p /etc/portage/package.unmask
if [[ -n "${DOCKER_PACKAGE_UNMASK}" ]] ; then
	echo "${DOCKER_PACKAGE_UNMASK}" >> /etc/portage/package.unmask/specific
	echo "package.unmask :"
	echo "${DOCKER_PACKAGE_UNMASK}"
	echo
fi
if [[ "${#DOCKER_IGNORE_GLSAS[@]}" -gt 0 ]] ; then
	echo "Following GLSAs will be ignored :"
	echo "${DOCKER_IGNORE_GLSAS[*]}"
	echo
fi
if [[ -n "${DOCKER_MAKEOPTS}" ]] ; then
	echo 'MAKEOPTS="'"${DOCKER_MAKEOPTS}"'"' >> /etc/portage/make.conf
	echo "MAKEOPTS :"
	echo "${DOCKER_MAKEOPTS}"
	echo
fi
if [[ "${DOCKER_DISTCC_ENABLED}" -eq 1 ]] ; then
	echo "Distcc enabled :"
	echo "Hosts : '${DOCKER_DISTCC_HOSTS}'"
	echo "Makeopts : '${DOCKER_DISTCC_MAKEOPTS}'"
	distcc-config --set-hosts "${DOCKER_DISTCC_HOSTS}"
	if [[ -n "${DOCKER_DISTCC_MAKEOPTS}" ]] ; then
		echo 'MAKEOPTS="'"${DOCKER_DISTCC_MAKEOPTS}"'"' >> /etc/portage/make.conf
	fi
	echo 'FEATURES="''$''{FEATURES} distcc"' >> /etc/portage/make.conf
	echo
fi
if [[ -n "${DOCKER_APPEND_MAKE_CONF}" ]] ; then
	echo "${DOCKER_APPEND_MAKE_CONF}" >> /etc/portage/make.conf
	echo "Additional make.conf config :"
	echo "${DOCKER_APPEND_MAKE_CONF}"
	echo
fi
emerge --info
IFS="${oIFS}"

mkdir -p /etc/portage/profile
portage_profile_packages > /etc/portage/profile/packages

# Emerge to / first
cat << 'EOF'
#######################
# (1/7) Emerging to / #
#######################
EOF

emerge --autounmask \
	--autounmask-write \
	--autounmask-continue \
	--noreplace \
	--newuse \
	--quiet-build \
	--verbose \
	"${DOCKER_BUILDER_PACKAGES[@]}" \
	"${DOCKER_TARGET_PACKAGES[@]}"
etc-update

# GLSA check
cat << 'EOF'
####################
# (2/7) GLSA check #
####################
EOF

glsa-check -t all | tee /tmp/glsa-check
if [[ -s /tmp/glsa-check ]]
then
	# shellcheck disable=SC2046
	glsa-check -p $(</tmp/glsa-check)
	for glsa in "${DOCKER_IGNORE_GLSAS[@]}" ; do
		if grep -q "${glsa}" /tmp/glsa-check ; then
			echo "WARNING : Ignoring '${glsa}'"
			sed -i "/${glsa}/d" /tmp/glsa-check
		fi
	done
	# shellcheck disable=SC2046
	[[ -s /tmp/glsa-check ]] && glsa-check -f $(</tmp/glsa-check)
fi
rm -f /tmp/glsa-check

# Emerge to DOCKER_TARGET_DIR using the binary packages
cat << 'EOF'
################################
# (3/7) Emerging to target dir #
################################
EOF

mkdir -p "${DOCKER_TARGET_DIR}"
emerge --noreplace \
	--quiet-build \
	--root="${DOCKER_TARGET_DIR}" \
	--root-deps=rdeps \
	--usepkgonly \
	--verbose \
	--with-bdeps=n \
	"${DOCKER_NEEDED_BASE_PACKAGES[@]}" \
	"${DOCKER_TARGET_PACKAGES[@]}"

# Copy needed gcc libraries
cp -va \
	/usr/lib/gcc/*/*/libatomic.so* \
	/usr/lib/gcc/*/*/libgcc_s.so* \
	/usr/lib/gcc/*/*/libgomp.so* \
	/usr/lib/gcc/*/*/libstdc++.so* \
	"${DOCKER_TARGET_DIR}"/usr/lib/


# Remove unneeded stuff
cat << 'EOF'
##########################
# (4/7) Deep depcleaning #
##########################
EOF

mkdir -p /etc/portage/profile
for f in $(ls -d1 "${DOCKER_TARGET_DIR}"/var/db/pkg/*/* | \
	grep -e '/portage' \
		-e '/eselect' \
		-e '/perl-cleaner' \
		-e '/metalog' \
		-e '/logger' \
		-e '/pkgconfig' \
		-e '/gentoo-functions')
do
	echo "$(basename "$(dirname "${f}")")/$(basename "${f}")"
done >> /etc/portage/profile/package.provided
emerge --root="${DOCKER_TARGET_DIR}" \
	--root-deps=rdeps \
	--with-bdeps=n \
	--verbose \
	--quiet-build \
	--depclean


# Verify that executables do not have broken libraries
cat << 'EOF'
####################################
# (5/7) Check for broken libraries #
####################################
EOF

mapfile -t BROKEN_LIBS < <(find "${DOCKER_TARGET_DIR}"/{bin,sbin,usr/bin,usr/sbin} \
	-executable \
	-type f \
	-exec sh -c 'readelf -d "${1}" &>/dev/null && f="${1#'"${DOCKER_TARGET_DIR}"'}" && chroot "'"${DOCKER_TARGET_DIR}"'" /usr/bin/ldd "${f}" 2>/dev/null | awk "/not found/ {print \$1}"' _ {} \; | \
	sort -u)

if [[ "${#BROKEN_LIBS[@]}" -gt 0 ]] ; then
	echo "Found broken libs :" "${BROKEN_LIBS[@]}"
	echo
	mapfile -t BROKEN_LIBS_PKGS < <(equery --quiet belongs --name-only "${BROKEN_LIBS[@]}")
	if [[ "${#BROKEN_LIBS_PKGS[@]}" -gt 0 ]] ; then
		echo "Emerging : " "${BROKEN_LIBS_PKGS[@]}"
		echo
		emerge --noreplace \
			--quiet-build \
			--root="${DOCKER_TARGET_DIR}" \
			--root-deps=rdeps \
			--usepkgonly \
			--verbose \
			--with-bdeps=n \
			"${BROKEN_LIBS_PKGS[@]}"
	fi
fi

cat << 'EOF'
#######################################
# (6/7) Delete gentoo metainformation #
#######################################
EOF

# Remove the gentoo metainformation
rm -vrf \
	"${DOCKER_TARGET_DIR}"/var/db/pkg \
	"${DOCKER_TARGET_DIR}"/var/lib/gentoo \
	"${DOCKER_TARGET_DIR}"/var/lib/portage

# Also clean out the distfiles
rm -vrf /usr/portage/distfiles/*

# Install busybox links
cat << 'EOF'
#########################
# (7/7) Install busybox #
#########################
EOF

chroot "${DOCKER_TARGET_DIR}" \
	/bin/busybox sh -c 'mkdir -p /tmp/bin && /bin/busybox --install -s /tmp/bin && mv -n /tmp/bin/* /bin && rm -rf /tmp/bin'

# Handle /usr/bin/env
if [[ ! -f "${DOCKER_TARGET_DIR}"/usr/bin/env ]] ; then
	chroot "${DOCKER_TARGET_DIR}" /bin/busybox sh -c 'ln -s "''$''(which env)" /usr/bin/env'
fi

echo ">>> Image ready in '${DOCKER_TARGET_DIR}'"
