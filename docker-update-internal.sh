#!/usr/bin/env bash
# This script is supposed to be run in the docker context
# It should not source any dependencies
# This updates the gentoo image to the latest version
IFS=$'\n\t'
set -euo pipefail

env-update
# shellcheck disable=SC1091
source /etc/profile
layman -S
eix-sync
eselect news read
emerge --update --verbose --deep --newuse --quiet-build --usepkg=n @world
etc-update
emerge --depclean
env-update
source /etc/profile
emerge --verbose --quiet-build --usepkg=n --oneshot @changed-deps @preserved-rebuild
# GLSA check
glsa-check -t all | tee /tmp/glsa-check
if [[ -s /tmp/glsa-check ]]
then
	# shellcheck disable=SC2046
	glsa-check -p $(</tmp/glsa-check)
	# shellcheck disable=SC2046
	glsa-check -f $(</tmp/glsa-check)
fi
rm -f /tmp/glsa-check
etc-update
env-update
emaint all || true
eclean-pkg -d
rm -rf /usr/portage/distfiles/*
echo "Tree version $(date +'%Y%m%d%H%M%S' \
	--date="$(head -n 1 /usr/portage/metadata/timestamp.commit | \
	cut -d' ' -f 3)")"
