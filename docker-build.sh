#!/usr/bin/env bash
IFS=$'\n\t'
set -euo pipefail

SOURCE_DIR="$(dirname "$(readlink -f "${0:-.}")")"

source "${SOURCE_DIR}"/common.sh
source "${SOURCE_DIR}"/config.sh
BUILDER_IMAGE_NAME="${IMAGE_NAME:-}"
BUILDER_IMAGE_VERSION="$(get_image_version_from_image_name "${BUILDER_IMAGE_NAME}")"
source ./config.sh

IMAGE_NAME="${IMAGE_NAME:-}"
IMAGE_VERSION="${IMAGE_VERSION:-}-${BUILDER_IMAGE_NAME##*builder-}-${BUILDER_IMAGE_VERSION}"
IMAGE_BUILDER_NAME="${IMAGE_NAME}-builder"
IMAGE_BUILDER_VERSION="${IMAGE_VERSION}"
IMAGE_BUILDER_INTERMEDIATE_NAME="${IMAGE_BUILDER_NAME}-intermediate"
IMAGE_BUILDER_INTERMEDIATE_VERSION="${IMAGE_VERSION}"

DOCKER_TARGET_DIR="${DOCKER_TARGET_DIR:-/mnt/target}"
DOCKER_USE=("${DOCKER_USE[@]}")
DOCKER_APPEND_MAKE_CONF="${DOCKER_APPEND_MAKE_CONF:-}"
DOCKER_PACKAGE_ACCEPT_KEYWORDS="${DOCKER_PACKAGE_ACCEPT_KEYWORDS:-}"
DOCKER_PACKAGE_USE="${DOCKER_PACKAGE_USE:-}"
DOCKER_PACKAGE_UNMASK="${DOCKER_PACKAGE_UNMASK:-}"
DOCKER_TARGET_PACKAGES=("${DOCKER_TARGET_PACKAGES[@]}")
DOCKER_BUILDER_PACKAGES=("${DOCKER_BUILDER_PACKAGES[@]}")
DOCKER_IGNORE_GLSAS=("${DOCKER_IGNORE_GLSAS[@]}")

# makeopts
DOCKER_MAKEOPTS="${DOCKER_MAKEOPTS:-}"

# distcc
GLOBAL_DOCKER_DISTCC_ENABLED="${DOCKER_DISTCC_ENABLED:-}"
GLOBAL_DOCKER_DISTCC_HOSTS="${DOCKER_DISTCC_HOSTS:-}"
GLOBAL_DOCKER_DISTCC_MAKEOPTS="${DOCKER_DISTCC_MAKEOPTS:-}"
source "${SOURCE_DIR}"/distcc.sh
DOCKER_DISTCC_ENABLED="${GLOBAL_DOCKER_DISTCC_ENABLED:-${DOCKER_DISTCC_ENABLED:-0}}"
DOCKER_DISTCC_HOSTS="${GLOBAL_DOCKER_DISTCC_HOSTS:-${DOCKER_DISTCC_HOSTS}}"
DOCKER_DISTCC_MAKEOPTS="${GLOBAL_DOCKER_DISTCC_MAKEOPTS:-${DOCKER_DISTCC_MAKEOPTS}}"

# Use builder to emerge environment
if [[ -z "$(get_image_id_from_image_name \
	"${IMAGE_BUILDER_NAME}:${IMAGE_BUILDER_VERSION}")" ]] || \
	[[ "$(sha1sum ./config.sh | cut -d' ' -f 1)" != "$(docker inspect \
		--type=image \
		--format='{{index .Config.Labels "fr.rezom.docker.config-sha1"}}' \
		"${IMAGE_BUILDER_NAME}:${IMAGE_BUILDER_VERSION}" 2>/dev/null)" ]]; then
	patches=()
	[[ -d ./patches ]] && patches+=(-v "$(readlink -f ./patches):/etc/portage/patches:ro")
	echo ">>> Building base builder image '${IMAGE_BUILDER_NAME}:${IMAGE_BUILDER_VERSION}'"
	# Build the needed packages in the container
	oIFS="${IFS}" ; IFS=' '
	docker_run_build \
		"${patches[@]}" \
		-e "DOCKER_TARGET_DIR=${DOCKER_TARGET_DIR}" \
		-e "DOCKER_USE=${DOCKER_USE[*]}" \
		-e "DOCKER_APPEND_MAKE_CONF=${DOCKER_APPEND_MAKE_CONF}" \
		-e "DOCKER_PACKAGE_ACCEPT_KEYWORDS=${DOCKER_PACKAGE_ACCEPT_KEYWORDS}" \
		-e "DOCKER_PACKAGE_USE=${DOCKER_PACKAGE_USE}" \
		-e "DOCKER_PACKAGE_UNMASK=${DOCKER_PACKAGE_UNMASK}" \
		-e "DOCKER_TARGET_PACKAGES=${DOCKER_TARGET_PACKAGES[*]}" \
		-e "DOCKER_BUILDER_PACKAGES=${DOCKER_BUILDER_PACKAGES[*]}" \
		-e "DOCKER_IGNORE_GLSAS=${DOCKER_IGNORE_GLSAS[*]}" \
		-e "DOCKER_MAKEOPTS=${DOCKER_MAKEOPTS}" \
		-e "DOCKER_DISTCC_ENABLED=${DOCKER_DISTCC_ENABLED}" \
		-e "DOCKER_DISTCC_HOSTS=${DOCKER_DISTCC_HOSTS}" \
		-e "DOCKER_DISTCC_MAKEOPTS=${DOCKER_DISTCC_MAKEOPTS}" \
		"${BUILDER_IMAGE_NAME}:${BUILDER_IMAGE_VERSION}" bash < "${SOURCE_DIR}"/docker-build-internal.sh
	IFS="${oIFS}"

	# Commit the image
	container_id="$(get_container_id_from_ancestor "${BUILDER_IMAGE_NAME}:${BUILDER_IMAGE_VERSION}")"
	docker_commit \
		"${container_id}" \
		"${IMAGE_BUILDER_NAME}:${IMAGE_BUILDER_VERSION}"
	docker rm "${container_id}"
fi

# If there's an intermediate Dockerfile, check if we need to rebuild
if [[ -f Dockerfile.intermediate  ]] ; then
if [[ -z "$(get_image_id_from_image_name \
	"${IMAGE_BUILDER_INTERMEDIATE_NAME}:${IMAGE_BUILDER_INTERMEDIATE_VERSION}")" ]] || \
	[[  "$(docker inspect \
		--type=image \
		--format='{{index .Config.Labels "fr.rezom.docker.config-sha1"}}' \
		"${IMAGE_BUILDER_NAME}:${IMAGE_BUILDER_VERSION}" 2>/dev/null)" != \
		"$(docker inspect \
		--type=image \
		--format='{{index .Config.Labels "fr.rezom.docker.config-sha1"}}' \
		"${IMAGE_BUILDER_INTERMEDIATE_NAME}:${IMAGE_BUILDER_INTERMEDIATE_VERSION}" 2>/dev/null)" ]] || \
	[[ "$(sha1sum Dockerfile.intermediate | cut -d' ' -f 1)" != "$(docker inspect \
		--type=image \
		--format='{{index .Config.Labels "fr.rezom.docker.dockerfile-sha1"}}' \
		"${IMAGE_BUILDER_INTERMEDIATE_NAME}:${IMAGE_BUILDER_INTERMEDIATE_VERSION}" 2>/dev/null)" ]]; then
	echo ">>> Building intermediate builder image '${IMAGE_BUILDER_INTERMEDIATE_NAME}:${IMAGE_BUILDER_INTERMEDIATE_VERSION}'"
	DOCKERFILE="Dockerfile.intermediate" \
		IMAGE_NAME="${IMAGE_BUILDER_INTERMEDIATE_NAME}" \
		IMAGE_VERSION="${IMAGE_BUILDER_INTERMEDIATE_VERSION}" \
		docker_build \
			--build-arg=DOCKER_TARGET_DIR="${DOCKER_TARGET_DIR}"
fi
fi

# Build from the Dockerfile
echo ">>> Building final image '${IMAGE_NAME}:${IMAGE_VERSION}'"
docker_build \
	--build-arg=DOCKER_TARGET_DIR="${DOCKER_TARGET_DIR}" \
	--squash

echo ">>> Image is ready at '${IMAGE_NAME}:${IMAGE_VERSION}'"
